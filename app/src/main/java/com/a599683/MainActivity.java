package com.a599683;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//активность при запуске приложения(ввод пароля)
public class MainActivity extends AppCompatActivity {
    //загружаем native-lib.so, написанную нами
    static {
        System.loadLibrary("native-lib");
    }

    private SharedPreferences sharedPreferences;//настройки
    private EditText passwordEditText;//поле для ввода пароля
    private Button passwordButton;//кнопка для входа

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);//получаем настройки
        if (!sharedPreferences.contains("password")) {//если нет настройки "password",то создаём её и пароль по умолчанию - 1234
            sharedPreferences.edit().putString("password", "1234").apply();
        }
        passwordButton = (Button) findViewById(R.id.passwordButton);//получаем кнопку
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);//получаем поле ввода
        final MainActivity mainActivity = this;//запоминаем объект MainActivity для смены активности
        passwordButton.setOnClickListener(new View.OnClickListener() {//обработчик кнопки
            @Override
            public void onClick(View v) {
                String password = String.valueOf(passwordEditText.getText());//получаем введённый пароль
                if (password.equals(sharedPreferences.getString("password", "1234"))) {//если он совпадает с тем,который в настройках
                    passwordEditText.setText("");//очищаем поле ввода
                    Intent intent = new Intent(mainActivity, MenuActivity.class);//создаёи намерение для смены активности на MenuActivity
                    mainActivity.startActivity(intent);//меняем активность
                } else {//иначе
                    Toast.makeText(mainActivity, "Пароль неверный!", Toast.LENGTH_SHORT).show();//говорим,что пароль не верный
                }
            }
        });
    }

}
