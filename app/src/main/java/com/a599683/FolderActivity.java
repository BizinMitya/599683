package com.a599683;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

//активность списка файлов в каталоге crypt
public class FolderActivity extends AppCompatActivity {
    private static final String PATH_TO_SDCARD = "/storage/sdcard0/";//путь к SDCARD
    private ListView listView;//список файлов в папке crypt

    public native void decrypt(String from, String to);//нативный метод для дешифрования

    public native void encrypt(String from, String to);//нативный метод для шифрования

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);
        final FolderActivity folderActivity = this;//запоминаем объект this для смены активности в анонимном блоке
        listView = (ListView) findViewById(R.id.listView);//получаем список
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {//обработчик нажатия на элемент списка(нужный файл)
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                String from = PATH_TO_SDCARD + "crypt/" + ((TextView) itemClicked).getText();//полный путь к файлу для шифрования
                //полный путь, куда запишется расшифрованный файл(метка,что файл шифрованный - _enc перед расширением,поэтому удаляем это из названия файла)
                String to = PATH_TO_SDCARD + "decrypt/" + String.valueOf(((TextView) itemClicked).getText()).replace("_enc.", ".");
                decrypt(from, to);//вызываем нативную функцию дешифрования
                Toast.makeText(folderActivity, "Файл " + ((TextView) itemClicked).getText() + " расшифрован!", Toast.LENGTH_SHORT).show();//уведомляем пользователя,что файл расшифрован
            }
        });
        File cryptDir = new File(PATH_TO_SDCARD + "crypt");//вирутальный файл директории crypt
        if (!cryptDir.exists()) {//если нет такой директории
            cryptDir.mkdir();//создаём её
        }
        File[] cryptFiles = cryptDir.listFiles();//список файлов в папке crypt
        for (File file : cryptFiles) {
            if (!file.isDirectory()) {//если файл не директория
                if (!file.getName().contains("_enc.")) {//если файл не шифрован(метка - _enc в названии файла перед расширением)
                    encrypt(PATH_TO_SDCARD + "crypt/" + file.getName(), PATH_TO_SDCARD + "crypt/" + file.getName().replace(".", "_enc."));//то вызываем нативную функцию шифрования(в этой же директории создаётся файл с _enc в конце)
                    file.delete();//удаляем нешифрованный файл
                }
            }
        }
        File decryptDir = new File(PATH_TO_SDCARD + "decrypt");//вирутальный файл директории decrypt
        if (!decryptDir.exists()) {//если нет такой директории
            decryptDir.mkdir();//создаём её
        }
        List<String> cryptFilesList = new ArrayList<>();//список названий файлов в директории crypt
        cryptFiles = cryptDir.listFiles();//снова получаем список файлов директории crypt,но теперь они уже шифрованные
        for (File file : cryptFiles) {
            if (!file.isDirectory()) {//проверяем,чтобы не было каталогов
                cryptFilesList.add(file.getName());//добавляем название в список
            }
        }
        String[] cryptFilesNames = new String[cryptFilesList.size()];//массив из названий файлов для адаптера
        for (int i = 0; i < cryptFilesNames.length; i++) {
            cryptFilesNames[i] = cryptFilesList.get(i);
        }
        // используем адаптер данных для отображения названий файлов в директории crypt
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, cryptFilesNames);
        listView.setAdapter(adapter);//устанавливаем адаптер
    }

}
