package com.a599683;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

//активность после ввода пароля(выбор - в настройки или в список файлов)
public class MenuActivity extends AppCompatActivity {
    private Button passwordButton;//кнопка для смены пароля
    private Button folderButton;//кнопка для спсика файлов

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        folderButton = (Button) findViewById(R.id.folderButton);
        passwordButton = (Button) findViewById(R.id.passwordButton);
        final MenuActivity menuActivity = this;
        folderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity, FolderActivity.class);
                startActivity(intent);
            }
        });
        passwordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(menuActivity, SettingsActivity.class);
                startActivity(intent);
            }
        });
    }
}
