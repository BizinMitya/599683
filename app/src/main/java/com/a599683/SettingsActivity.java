package com.a599683;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//активность для смены пароля
public class SettingsActivity extends AppCompatActivity {
    private EditText settingsEditText;//поле для ввода нового пароля
    private Button settingsButton;//кнопка сохранения нового пароля
    private SharedPreferences sharedPreferences;//настройки

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        settingsEditText = (EditText) findViewById(R.id.settingsEditText);
        settingsButton = (Button) findViewById(R.id.settingsButton);
        sharedPreferences = getSharedPreferences("settings", MODE_PRIVATE);
        final SettingsActivity settingsActivity = this;
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences.edit().putString("password", String.valueOf(settingsEditText.getText())).apply();
                settingsEditText.setText("");
                Toast.makeText(settingsActivity, "Новый пароль сохранён!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
